From alpine:3.13

EXPOSE 25 587 465

RUN apk --no-cache add \
	openssl \
	ca-certificates \
	postfix \
	postfix-ldap

ADD rootfs /

CMD ["entrypoint.sh"]