#!/bin/sh

function setVal {
	KEY="$1"
	VALUE="$2"
	FILE="$3"
	echo "$FILE: $KEY=$VALUE"
	ESCAPED=$(printf '%s\n' "$VALUE" | sed -e 's/[\/&]/\\&/g')
	sed -i "s/^#\?\s*$KEY\s*=.*\$/$KEY=$ESCAPED/" $FILE
}

#########################################
# Setup conf
#########################################

if [ -n "$MYHOSTNAME" ]; then
	sed -i "/virtual.domain.tld/d" /etc/postfix/main.cf
	setVal "myhostname" "$MYHOSTNAME" /etc/postfix/main.cf
fi

if [ -n "$MYDOMAIN" ]; then
	setVal "mydomain" "$MYDOMAIN" /etc/postfix/main.cf
fi

if [ -n "$LDAP_SERVER_URI" ]; then
	setVal "server_host" "$LDAP_SERVER_URI" /etc/postfix/virtual_alias_domains
	setVal "server_host" "$LDAP_SERVER_URI" /etc/postfix/virtual_alias_maps
fi

if [ -n "$LDAP_BIND_DN" ]; then
	setVal "bind_dn" "$LDAP_BIND_DN" /etc/postfix/virtual_alias_domains
	setVal "bind_dn" "$LDAP_BIND_DN" /etc/postfix/virtual_alias_maps
fi

if [ -n "$LDAP_BIND_PW" ]; then
	setVal "bind_pw" "$LDAP_BIND_PW" /etc/postfix/virtual_alias_domains
	setVal "bind_pw" "$LDAP_BIND_PW" /etc/postfix/virtual_alias_maps
fi

if [ -n "$LDAP_SEARCH_BASE" ]; then
	setVal "search_base" "$LDAP_SEARCH_BASE" /etc/postfix/virtual_alias_domains
	setVal "search_base" "$LDAP_SEARCH_BASE" /etc/postfix/virtual_alias_maps
fi

if [ -n "$MDA_LMTP_ADDR" ]; then
	setVal "virtual_transport" "lmtp:inet:$MDA_LMTP_ADDR" /etc/postfix/main.cf
fi

if [ -n "$DOVECOT_SASL_ADDR" ]; then
	setVal "smtpd_sasl_path" "inet:$DOVECOT_SASL_ADDR" /etc/postfix/main.cf
fi

if [ -n "$POSTFIX_SSL_CERT" ]; then
	setVal "smtpd_tls_cert_file" "$POSTFIX_SSL_CERT" /etc/postfix/main.cf
fi

if [ -n "$POSTFIX_SSL_KEY" ]; then
	setVal "smtpd_tls_key_file" "$POSTFIX_SSL_KEY" /etc/postfix/main.cf
fi

if [ -n "$DKIM_MILTER" ]; then
	setVal "dkim_milter" "$DKIM_MILTER" /etc/postfix/main.cf
	setVal "smtpd_milters" "\$dkim_milter" /etc/postfix/main.cf
	setVal "non_smtpd_milters" "\$dkim_milter" /etc/postfix/main.cf
fi

if [ -n "$RELAY_HOST" ] && [ -n "$RELAY_PORT" ] && [ -n "$RELAY_USER" ] && [ -n "$RELAY_PASS" ]; then
	setVal "relayhost" "[$RELAY_HOST]:$RELAY_PORT" /etc/postfix/main.cf
	echo "[$RELAY_HOST]:$RELAY_PORT $RELAY_USER:$RELAY_PASS" > /etc/postfix/sasl_passwd
	postmap /etc/postfix/sasl_passwd
	chown root:postfix /etc/postfix/sasl_passwd*
	chmod 640 /etc/postfix/sasl_passwd*
fi

# if [ ! -f /etc/postfix/aliases.lmdb ]; then
# 	postmap /etc/postfix/aliases
# fi

exec postfix start-fg